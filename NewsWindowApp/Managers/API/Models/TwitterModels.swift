//
//  TwitterModels.swift
//  NewsWindowApp
//
//  Created by Яков on 28.11.2021.
//

import Foundation
import SwiftUI

struct TwitterResponse: Codable {
    let data: TwitterProfileAPI
}

struct TwitterProfileAPI: Codable {
    enum CodingKeys: String, CodingKey {
        case userInfo = "user_info"
        case id
    }
    let id: String
    let userInfo: TwitterProfileInfo
}

struct TwitterProfileInfo: Codable {
    enum CodingKeys: String, CodingKey {
        case profileImage = "profile_image_url_https"
        
    }
    let profileImage: String
}
