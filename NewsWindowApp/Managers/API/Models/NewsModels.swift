//
//  NewsModels.swift
//  NewsWindowApp
//
//  Created by Яков on 25.11.2021.
//

import Foundation

struct NewsResponse: Codable {
    enum CodingKeys: String, CodingKey {
        case status, page, articles
        case totalHits = "total_hits"
        case totalPages = "total_pages"
        case pageSize = "page_size"
        case userInput = "user_input"
    }
    let status: String
    let totalHits: Int
    let page: Int
    let totalPages: Int
    let pageSize: Int
    let articles: [NewsAPI]
    let userInput: UserInput
}

struct NewsAPI: Codable {
    enum CodingKeys: String, CodingKey {
        case title, author, link, summary, media, authors, topic
        case publishedDate = "published_date"
        case twitterAccount = "twitter_account"
        case id = "_id"
    }
    let title: String?
    let author: String?
    let publishedDate: String?
    let link: String?
    let summary: String?
    let topic: String?
    let authors: [String]
    let media: String?
    let twitterAccount: String?
    let id: String?
}

struct UserInput: Codable {
    var lang: String?
    var page: Int
    var size: Int
}

