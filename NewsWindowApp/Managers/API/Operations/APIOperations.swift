//
//  loadNews.swift
//  NewsWindowApp
//
//  Created by Яков on 25.11.2021.
//

import Foundation

class APIOperations {
    
    //MARK: - loading News
    func loadNews(search: String, pagination: UserInput, completionHandler: @escaping (NewsResponse?, String?) -> Void) {
        let parameters: [String: Any] = ["q": search, "lang": "", "page_size": pagination.size, "page": pagination.page]
        
        NetworkManager.shared.networkRequestFreeNews(urlPath: URLs.freeNewsURL, method: .get, parameters: parameters) { data, error in
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(NewsResponse.self, from: data)
                completionHandler(response, nil)
                
            } catch let errorDecode as NSError { completionHandler(nil, errorDecode.localizedDescription) }
        }
    }
    
    //MARK: - loading Twitter profile
    func loadTwitterProfile(twitterAccount: String, completionHandler: @escaping (TwitterProfileAPI?) -> Void) {
        let userName: String = String(twitterAccount.dropFirst())
        let parameters: [String: Any] = ["username": userName]
        
        NetworkManager.shared.networkRequestTwitter(urlPath: URLs.twitterURL, method: .get, parameters: parameters) { data, error in
            guard error == nil else {
                completionHandler(nil)
                print(error ?? "error")
                return
            }
            
            guard let data = data else {
                completionHandler(nil)
                print("Unknown error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(TwitterResponse.self, from: data)
                completionHandler(response.data)
                
            } catch let errorDecode as NSError {
                completionHandler(nil)
                print(errorDecode.localizedDescription)
            }
        }
    }
    
}
