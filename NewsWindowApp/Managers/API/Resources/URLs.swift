//
//  URLs.swift
//  NewsWindowApp
//
//  Created by Яков on 25.11.2021.
//

import Foundation

struct URLs {
    static let freeNewsURL = "https://free-news.p.rapidapi.com/v1/search"
    static let twitterURL = "https://twitter32.p.rapidapi.com/getProfile"
}
