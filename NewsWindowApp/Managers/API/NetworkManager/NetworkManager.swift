//
//  NetworkManager.swift
//  NewsWindowApp
//
//  Created by Яков on 25.11.2021.
//

import Foundation
import Alamofire
import UIKit

class NetworkManager {
    
    static let shared = NetworkManager()

    let freeNewsAPIHost = "free-news.p.rapidapi.com"
    let freeNewsAPIKey = "251b4eb8c7mshc148c7ffba6d3c8p1ff79fjsnc7edda17d876"
    let twitterAPIHost = "twitter32.p.rapidapi.com"
    let twitterAPIKey = "251b4eb8c7mshc148c7ffba6d3c8p1ff79fjsnc7edda17d876"
    
    //MARK: - Request Free News
    func networkRequestFreeNews(urlPath: String,
                         method: HTTPMethod,
                     parameters: [String: Any]? = nil,
                        headers: [String: String]? = nil,
              completionHandler: @escaping (Data?, String?) -> Void) {
        
        if NetworkReachabilityManager()?.isReachable ?? false {
            var staticHTTPHeaders: [String: String] = ["x-rapidapi-host": freeNewsAPIHost, "x-rapidapi-key": freeNewsAPIKey]
            var httpHeaders: [HTTPHeader] = []
            
            if let headers = headers {
                for (key, value) in headers {
                    staticHTTPHeaders[key] = value
                }
            }
            
            staticHTTPHeaders.forEach { key, value in
                let httpHeader = HTTPHeader(name: key, value: value)
                httpHeaders.append(httpHeader)
            }
            
            AF.request(urlPath,
                       method: method,
                   parameters: parameters,
                      headers: HTTPHeaders(httpHeaders)).response { response in
                
                switch response.result {
                case .success(let data):
                    completionHandler(data, nil)
                case .failure(let error):
                    completionHandler(nil, error.localizedDescription)
                }
            }
        } else {
            completionHandler(nil, "No internet connection...")
        }
        
    }
    
    //MARK: - Request Twitter
    func networkRequestTwitter(urlPath: String,
                         method: HTTPMethod,
                     parameters: [String: Any]? = nil,
                        headers: [String: String]? = nil,
              completionHandler: @escaping (Data?, String?) -> Void) {
        
        var staticHTTPHeaders: [String: String] = ["x-rapidapi-host": twitterAPIHost, "x-rapidapi-key": twitterAPIKey]
        var httpHeaders: [HTTPHeader] = []
        
        if let headers = headers {
            for (key, value) in headers {
                staticHTTPHeaders[key] = value
            }
        }
        
        staticHTTPHeaders.forEach { key, value in
            let httpHeader = HTTPHeader(name: key, value: value)
            httpHeaders.append(httpHeader)
        }
        
        AF.request(urlPath,
                   method: method,
               parameters: parameters,
                  headers: HTTPHeaders(httpHeaders)).response { response in
            
            switch response.result {
            case .success(let data):
                completionHandler(data, nil)
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
}
