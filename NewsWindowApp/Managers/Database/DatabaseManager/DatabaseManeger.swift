//
//  DatabaseManeger.swift
//  NewsWindowApp
//
//  Created by Яков on 30.11.2021.
//

import Foundation
import RealmSwift

class DatabaseManager {

    static let shared = DatabaseManager()
    private let realm = try? Realm()

    // MARK: - Save and get Database Realm
    func saveData(_ data: [NewsAPI]) {
        let data = transformDatabaseModel(data)
        do {
            try realm?.write({
                realm?.add(data)
            })
        } catch(let error) { print(error.localizedDescription) }
    }

    func getData() -> [News]? {
        guard let databaseResponse = realm?.objects(DatabaseNewsList.self).first else { return nil }
        return transformDataModel(databaseResponse)
    }

    // MARK: - Transform to Realm Model (DatabaseNewsList)
    private func transformDatabaseModel(_ data: [NewsAPI]) -> DatabaseNewsList {
        let databaseNewsList: DatabaseNewsList = DatabaseNewsList()
        let newsList: List<DatabaseNews> = List<DatabaseNews>()

        for item in data {
            let news: DatabaseNews = DatabaseNews()
            
            news.id = item.id
            news.title = item.title
            news.summary = item.summary
            news.media = item.media
            news.link = item.link
            news.topic = item.topic
            news.publishedDate = item.publishedDate
            news.author = item.author
            news.twitterAccount = item.twitterAccount
            
            let authorsList: List<Author> = List<Author>()
            item.authors.forEach { author in
                let authorNew: Author = Author()
                authorNew.author = author
                authorsList.append(authorNew)
            }
            news.authors?.authorsList = authorsList
            
            newsList.append(news)
        }
        databaseNewsList.newsList = newsList
        return databaseNewsList
    }

    // MARK: - Transform back to NewsModel
    private func transformDataModel(_ data: DatabaseNewsList) -> [News] {
        var newsList: [News] = []
        
        for item in data.newsList {
            var news: News = News(authors: [])
            
            news.id = item.id
            news.title = item.title
            news.summary = item.summary
            news.media = item.media
            news.link = item.link
            news.topic = item.topic
            news.publishedDate = item.publishedDate
            news.author = item.author
            news.twitterAccount = item.twitterAccount
            
            var authors: [String] = []
            item.authors?.authorsList.forEach { objc in
                authors.append(objc.author)
            }
            news.authors = authors
            
            newsList.append(news)
        }
        return newsList
    }
    
    //MARK: - clear realm data
    func realmDeleteAllObjects() {
        do {
            let realm = try Realm()
            let objects = realm.objects(DatabaseNewsList.self)
            
            try! realm.write {
                realm.delete(objects)
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }
    }
    
}
