//
//  DatabaseNewsModel.swift
//  NewsWindowApp
//
//  Created by Яков on 30.11.2021.
//

import Foundation
import RealmSwift

class DatabaseNewsList: Object {
    var newsList: List<DatabaseNews> = List<DatabaseNews>()
}

class DatabaseNews: Object {
    @objc dynamic var id: String? = ""
    @objc dynamic var title: String? = ""
    @objc dynamic var summary: String? = ""
    @objc dynamic var media: String? = ""
    @objc dynamic var link: String? = ""
    @objc dynamic var topic: String? = ""
    @objc dynamic var publishedDate: String? = ""
    @objc dynamic var author: String? = ""
    @objc dynamic var authors: AuthorsList? = AuthorsList()
    @objc dynamic var twitterAccount: String? = ""
}

class AuthorsList: Object {
    var authorsList: List<Author> = List<Author>()
}

class Author: Object {
    @objc dynamic var author: String = ""
}
