//
//  NewsWindowViewController.swift
//  NewsWindowApp
//
//  Created by Яков on 24.11.2021.
//

import UIKit

class NewsWindowViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    @IBOutlet weak var searchBarTopConstraint: NSLayoutConstraint!
    
    private let newsCellID = String(describing: NewsTableViewCell.self)
    private var totalPages = 0
    private var newsArray = [NewsAPI]()
    private var news = [News]()
    private var userImput = UserInput(lang: "", page: 1, size: 15)
    private var userImputTemp: UserInput? = nil
    private var searchText: String? = nil
    private var searchTextTemp: String? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        tableView.register(UINib(nibName: newsCellID, bundle: nil), forCellReuseIdentifier: newsCellID)
        loadNews(nextPage: false)
    }
    
    deinit { print("\n🏁 - \(classForCoder)\n") }
    
}

//MARK: - build table view
extension NewsWindowViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newsDetailsVC = storyBoard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as! NewsDetailsViewController
        newsDetailsVC.newsInfo = news[indexPath.row]
        self.navigationController?.pushViewController(newsDetailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: newsCellID, for: indexPath) as! NewsTableViewCell
        cell.update(news[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == news.count - 1 && userImput.page < totalPages {
            userImput.page += 1
            loadNews(nextPage: true, search: searchTextTemp ?? searchText)
        }
    }
    
}

//MARK: - work with Search Bar
extension NewsWindowViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBar.showsCancelButton = true
    }
    
    //MARK: - processing button actions Search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let search = searchBar.text {
            searchTextTemp = searchText
            userImputTemp = userImput
            userImput = UserInput(page: 1, size: 15)
            searchText = search.isEmpty ? searchText : search
            newsArray = []
            loadNews(nextPage: false, search: searchText)
        }
    }
    
    //MARK: - handling the actions of the Search Cancel Button
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        searchBar.showsCancelButton = false
        searchBar.text = searchTextTemp ?? ""
        searchBar.resignFirstResponder()
        searchText = searchTextTemp
        userImput = userImputTemp ?? UserInput(page: 1, size: 15)
        loadNews(nextPage: false, search: searchText)
        searchTextTemp = nil
    }
    
    //MARK: - the animated hiding Search Bar
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalMovement = tableView.contentOffset.y
        
        if verticalMovement >= 0 && verticalMovement <= searchBarHeight.constant {
            searchBarTopConstraint.constant = -verticalMovement
        }
    }

}

// MARK: - load News
extension NewsWindowViewController {
    
    private func loadNews(nextPage: Bool, search: String? = nil) {
        APIOperations().loadNews(search: search ?? "apple", pagination: userImput) { response, error in
            guard error == nil else {
                self.present(AlertView().showAlert(title: "Error", message: error, buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let response = response else {
                self.present(AlertView().showAlert(title: "Error", message: "No data from response", buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            nextPage ? (self.newsArray += response.articles) : (self.newsArray = response.articles)
            self.totalPages = response.totalPages
            self.userImput = response.userInput
            
            DatabaseManager.shared.realmDeleteAllObjects()
            DatabaseManager.shared.saveData(self.newsArray)
            self.news = DatabaseManager.shared.getData() ?? []
            self.tableView.reloadData()
        }
    }
    
}
