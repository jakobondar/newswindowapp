//
//  NewsTableViewCell.swift
//  NewsWindowApp
//
//  Created by Яков on 25.11.2021.
//

import UIKit
import Kingfisher

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func update(_ news: News) {
        titleLabel.text = news.title
        dataLabel.text = news.publishedDate
        guard let imageURL = news.media else {return}
        loadImage(urla: imageURL, imageView: pictureImageView)
    }
    
    private func loadImage(urla: String, imageView: UIImageView) {
        let url = URL(string: urla)
        imageView.kf.setImage(with: url)
    }
    
}



