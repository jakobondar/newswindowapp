//
//  NewsDetailsViewController.swift
//  NewsWindowApp
//
//  Created by Яков on 24.11.2021.
//

import UIKit
import Kingfisher

class NewsDetailsViewController: UIViewController {
    
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var twitterProfileImageView: UIImageView!
    @IBOutlet weak var twitterNikLabel: UILabel!
    @IBOutlet weak var authorsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var mediaImageViewHeightConstraint: NSLayoutConstraint!
    
    var newsInfo = News.init(authors: [])
    private var twitterID: String? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        twitterProfileImageView.layer.cornerRadius = twitterProfileImageView.frame.size.width / 2
        twitterProfileImageView.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mediaImageView.isHidden = false
        
        if let mediaURL = newsInfo.media {
            mediaImageViewHeightConstraint.constant = 210
            mediaImageView.isHidden = false
            
            let url = URL(string: mediaURL)
            mediaImageView.kf.setImage(with: url)
        } else { mediaImageView.isHidden = true; mediaImageViewHeightConstraint.constant = 0 }
        
        if let twitterAccount = newsInfo.twitterAccount {
            loadTwitterProfile(twitterAccount)
        }
        
        twitterNikLabel.text = newsInfo.twitterAccount
        authorsLabel.text = newsInfo.authors.isEmpty ? newsInfo.author : newsInfo.authors.joined(separator: ", ")
        titleLabel.text = newsInfo.title
        linkLabel.text = newsInfo.link
        summaryLabel.text = newsInfo.summary
        dataLabel.text = newsInfo.publishedDate
    }

    deinit { print("\n🏁 - \(classForCoder)\n") }
    
    @IBAction func switchingToTwitterAction(_ sender: Any) {
        guard let twitterID = twitterID, let url = URL(string: "https://twitter.com/intent/user?user_id=\(twitterID)") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func followTheLinkAction(_ sender: Any) {
        guard let link = newsInfo.link, let url = URL(string: link) else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - load Twitter Profile
extension NewsDetailsViewController {
    
    private func loadTwitterProfile(_ twitterAccount: String) {
        APIOperations().loadTwitterProfile(twitterAccount: twitterAccount) { response in
            guard let twitterAccount = response else {
                return
            }
            
            self.twitterID = twitterAccount.id
            let url = URL(string: twitterAccount.userInfo.profileImage)
            self.twitterProfileImageView.kf.setImage(with: url)
        }
    }
    
}
