//
//  LaunchScreenAnimationVC.swift
//  NewsWindowApp
//
//  Created by Яков on 30.11.2021.
//

import UIKit

class LaunchScreenAnimationVC: UIViewController {

    @IBOutlet weak var myLogoViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIView.animate(withDuration: 0.7,
                       delay: 0,
                       options: .curveEaseInOut,
                       
                       animations: {
            self.backgroundView.backgroundColor = .clear
            self.myLogoViewTopConstraint.constant = 45
            self.view.layoutIfNeeded() },
                       
                       completion: {_ in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newsWindowVC = storyBoard.instantiateViewController(withIdentifier: "NewsWindowViewController") as! NewsWindowViewController
            self.navigationController?.pushViewController(newsWindowVC, animated: false) })
    }

}
