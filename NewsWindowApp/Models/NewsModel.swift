//
//  NewsModel.swift
//  NewsWindowApp
//
//  Created by Яков on 28.11.2021.
//

import Foundation

struct NewsList {
    var newsList: [News]
}

struct News {
    var id: String?
    var title: String?
    var summary: String?
    var media: String?
    var link: String?
    var topic: String?
    var publishedDate: String?
    var author: String?
    var authors: [String]
    var twitterAccount: String?
}
